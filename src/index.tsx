import App from './components/App';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.scss';

ReactDOM.render(
	<React.StrictMode>
		<App/>
	</React.StrictMode>,
	document.getElementById('root')
);