import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import React from 'react';
import './TaskTileComponent.scss';
import {faPen, faTrash} from "@fortawesome/free-solid-svg-icons";

export interface TaskTileComponentProps {
	title: string;
	desc?: string;
	hour: string;
}

export function TaskTileComponent(props: TaskTileComponentProps) {
	return <div className='tasktile'>
		<div className='tasktile__wrapper'>
			<h5 className='tasktile__title'>{props.title}</h5>
			<p className='tasktile__desc'>{props.desc}</p>
			<div className='tasktile__bottom'>
				<p className='tasktile__hour'>{props.hour}</p>
				<FontAwesomeIcon className='tasktile__icon' icon={faPen} size='sm'/>
				<FontAwesomeIcon className='tasktile__icon' icon={faTrash} size='sm'/>
			</div>
		</div>
	</div>;
}