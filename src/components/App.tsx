import './App.scss';
import * as React from 'react';
import '../components/App.scss';
import {NavBar} from './NavBar';
import {CalendarComponent} from './CalendarComponent';
import {TasksComponent} from './TasksComponent';

export function App() {
	return (<>
			<NavBar/>
			<div className='main'>
				<CalendarComponent/>
				<TasksComponent/>
			</div>
		</>
	);
}

export default App;
